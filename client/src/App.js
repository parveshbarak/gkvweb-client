import React, {Component} from 'react';
import Layout from './core/Layout';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Lead from './components/LeadComponent';
import Main from './components/MainComponent';
import { isAuth, signout } from './auth/helpers';

class App extends Component {

 render() {
        return (
         <BrowserRouter>
          <div className="App">
            <Main />
          </div>
        </BrowserRouter>
    );
    }
    
};


export default App;