import React, {Component} from 'react';
import axios from 'axios';
import { toast } from 'react-toastify'

class Upload extends Component {
	constructor(props){    
    super(props);
    this.state = {
       selectedFile: null
    }
    this.imageSelectedHandler = this.imageSelectedHandler.bind(this);
  }


     imageSelectedHandler = event => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

     imageUploadHandler = event => {
        event.preventDefault();
        const fd = new FormData();
        if (this.state.selectedFile !== null) {
        fd.append('imageFile', this.state.selectedFile, this.state.selectedFile.name);
            axios.post(`${process.env.REACT_APP_API}/uploadimage`, fd)
            .then(res => {
                console.log(res);
                toast('Image Uploaded Sucessfully!')
            })
            .catch (err => {
                toast ('Please select an image first', { background: '#EE0022' })
                console.log(err)
            })
        } else {
            toast('Error Please seclect a file first')
        }
        
    }

    render ()  {
    	return(
    		<form>
	            <input type="file" name="imageFile" onChange={this.imageSelectedHandler} />
	            <button onClick={this.imageUploadHandler}> Upload </button>
	    	</form>
    	)
    }
}

export default Upload;