import React, { useState, useEffect, Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Layout from '../core/Layout';
import axios from 'axios';
import { isAuth, getCookie, signout, updateUser } from '../auth/helpers';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';

const Profile = ({ history }) => {
    const [values, setValues] = useState({
        role: '',
        name: '',
        email: '', 
        year: '',
        branch: '',
        description: '',
        password: '',
        image: '',
        linkedin: '',
        facebook: '',
        instagram: '',
        achievements : {
            image: '',
            about: ''
        } ,
        github: '',
        twitter: '',
        whatsapp: '000111000', 
        curricular: '',
        native: '',
        buttonText: 'Submit'
    });

    const token = getCookie('token');

    useEffect(() => {
        loadProfile();
    }, []);

    const loadProfile = () => {
        axios({
            method: 'GET',
            url: `${process.env.REACT_APP_API}/user/${isAuth()._id}`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => { 
                console.log('PRIVATE PROFILE UPDATE', response);
                const { role, name, email,year, image, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular, achievements } = response.data;
                setValues({ ...values, role, name, email, image, year, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular,achievements });
            })
            .catch(error => {
                console.log('PRIVATE PROFILE UPDATE ERROR', error.response.data.error);
                if (error.response.status === 401) {
                    signout(() => {
                        history.push('/');
                    });
                }
            });
    };

    const { role, name, email, password, year, image, branch, description, profileImg ,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular, achievements, buttonText } = values;

    

   const showDetail =() => (
            <React.Fragment>
                    <div className="row">
                        <div className="col-12 col-md-5 m-1">
                            <Card>
                                <CardImg src={image} height="450px" width="100px" alt={name} />
                                <CardBody>
                                    <CardTitle>{name}</CardTitle>
                                    <CardText>{description}</CardText>
                                </CardBody>
                            </Card>
                        </div>
                        <div className="col-12 col-md-5 m-1">
                           
                                    <h3>{name}</h3>
                                    <h5>--{branch} Department </h5>
                                    <h6 className="req-spacing">--{year} </h6>
                                    <p>Interest in Co-Curricular Activities :- {curricular}</p>
                                    <p> {name} belongs to <u> {native} </u></p>
                                    <ul>
                                        <li>Linkedin Profile :- {linkedin}</li>
                                        <li>Facebook Profile :- {facebook}</li>
                                        <li>Github Profile :- {github}</li>
                                        <li>Instagram Profile :- {instagram}</li>
                                        <li>Twitter Profile :- {twitter}</li>
                                        <li>Whatsapp No. :- {whatsapp}</li>
                                        <li> Achievement :-  {achievements.about}</li>
                                    </ul>
                        </div>
                    </div>
                </React.Fragment>
            );

    return (
        <Layout>
            <div>
                <ToastContainer />
                <h1 className="pt-5 text-center">Profile</h1>
                <p className="lead text-center">Profile Details</p>
                {showDetail()}
            </div>
        </Layout>
    );
};


export default Profile;