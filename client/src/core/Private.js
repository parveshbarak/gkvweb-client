import React, { useState, useEffect, Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Layout from '../core/Layout';
import axios from 'axios';
import { isAuth, getCookie, signout, updateUser } from '../auth/helpers';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import Upload from './uploadProfile'

const Private = ({ history }) => {
    const [values, setValues] = useState({
        role: '',
        name: '',
        email: '',
        year: '',
        branch: '',
        description: '',
        password: '',
        image: '',
        linkedin: '',
        facebook: '',
        instagram: '',
        achievements: {
            image: "",
            about: ""
        } ,
        github: '',
        twitter: '',
        whatsapp: '000111000',
        native: '', 
        curricular: '',
        buttonText: 'Submit' ,
    });

    const token = getCookie('token');

    useEffect(() => {
        loadProfile();
    }, []);

    const loadProfile = () => {
        axios({
            method: 'GET',
            url: `${process.env.REACT_APP_API}/user/${isAuth()._id}`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => { 
                console.log('PRIVATE PROFILE UPDATE', response);
                const { role, name, email, image, year, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular,achievements } = response.data;
                setValues({ ...values, role, name, email, image, year, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular, achievements });
            })
            .catch(error => {
                console.log('PRIVATE PROFILE UPDATE ERROR', error.response.data.error);
                if (error.response.status === 401) {
                    signout(() => {
                        history.push('/');
                    });
                }
            });
    };

    const { role, name, email, image, year, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular, achievements, password,buttonText } = values;

    const handleChange = name => event => {
        // console.log(event.target.value);
        setValues({ ...values, [name]: event.target.value });
    };

    const clickSubmit = event => {
        event.preventDefault();
        setValues({ ...values, buttonText: 'Submitting' });
        axios({
            method: 'PUT',
            url: `${process.env.REACT_APP_API}/user/update`,
            headers: {
                Authorization: `Bearer ${token}`
            },
            data: {role, name, year,image, branch, description, profileImg,linkedin,facebook,github,instagram,twitter,whatsapp,native,curricular, achievements, password }
        })
            .then(response => {
                console.log('PRIVATE PROFILE UPDATE SUCCESS', response);
                updateUser(response, () => {
                    setValues({ ...values, buttonText: 'Submitted' });
                    toast.success('Profile updated successfully, Now visit home Page!');
                });
            })
            .catch(error => {
                console.log('PRIVATE PROFILE UPDATE ERROR', error.response.data.error);
                setValues({ ...values, buttonText: 'Submit' });
                toast.error(error.response.data.error);
            });
    };


    const updateForm1 = () => (
        <form>

            <div className="form-group">
                <label className="text-muted">Linkedin Profile</label>
                <input onChange={handleChange('linkedin')} value={linkedin} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Facebook Profile</label>
                <input onChange={handleChange('facebook')} value={facebook} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Github Profile</label>
                <input onChange={handleChange('github')} value={github} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Instagram Profile</label>
                <input onChange={handleChange('instagram')} value={instagram} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Twitter Profile</label>
                <input onChange={handleChange('twitter')} value={twitter} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Whatsapp no.</label>
                <input onChange={handleChange('whatsapp')} value={whatsapp} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Native Place</label>
                <input onChange={handleChange('native')} value={native} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Co-CurricularActivities</label>
                <input onChange={handleChange('curricular')} value={curricular} type="text" className="form-control" />
            </div>
                   
            <div className="offset-1 mt-3" >
                <button className="btn btn-primary btn-block" onClick={clickSubmit}>
                    {buttonText}
                </button>
            </div>
            

        </form>
    );

    const updateForm2 = () => (
        <form>

            <div className="form-group">
                <label className="text-muted"><h3>Profile Image</h3></label>
                < Upload/> 
                <input onChange={handleChange('image')} value={image} type="text" className="form-control mt-2" />
            </div>

            <div className="form-group">
                <label className="text-muted">Email</label>
                <input defaultValue={email} type="email" className="form-control" disabled />
            </div>

            <div className="form-group">
                <label className="text-muted">Role</label>
                <input defaultValue={role} type="role" className="form-control" disabled />
            </div>
 
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input onChange={handleChange('name')} value={name} type="text" className="form-control" />
            </div>
 
            <div className="form-group">
                <label className="text-muted">Year/Qualification</label>
                <input onChange={handleChange('year')} value={year} type="text" className="form-control" />
            </div>
 
            <div className="form-group">
                <label className="text-muted">Branch</label>
                <input onChange={handleChange('branch')} value={branch} type="text" className="form-control" />
            </div>
 
            <div className="form-group">
                <label className="text-muted">Write Something About Yourself</label>
                <textarea rows="7" onChange={handleChange('description')} value={description} className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Password</label>
                <input onChange={handleChange('password')} value={password} type="password" className="form-control" />
            </div>

        </form>
        );

    

    return (
        <Layout>
                <h1 className="pt-5 text-center">Private</h1>
                <p className="lead text-center">Profile update</p>
        <div className="row">
            <div className="col-md-5">
                <ToastContainer />
                {updateForm2()}
            </div>
            <div className="col-md-1">
            </div>
            <div className="col-md-5">
                <ToastContainer />
                {updateForm1()}
            </div>
        </div>
        
            
       
        </Layout>
    );
};


export default Private;