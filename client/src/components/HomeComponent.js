import React, {Component} from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Media, Button  } from 'reactstrap';
import ExampleComponent from "react-rounded-image";
import { Link } from 'react-router-dom';
import { isAuth , authenticate } from '../auth/helpers';

class Home extends Component{
	//console.log(authenticate.user)
	constructor (props) {
		super(props);
		console.log(props.user)
	}
	render () {
		return(
			<React.Fragment>
				<div className="greeting">
					<h2>Hii !  </h2> 
				</div> 

				<div className="gallary">
					<h1>Here comes the gallary!</h1> <hr />
					    <div className="col-12 mt-5">
				            <Media tag="li">
				                <Media left middle>
				                    <Media top className=" mb-3 rounded-circle" object src= "/assets/images/faltu.jpg" height="100px" width="100px"  alt= "name" />
				        		</Media>
				        			<Media body className="col-12">
				                    <Media heading><h3>Prof. Roop Kishore Shastri</h3></Media>
				                    <h6>Vice-Chancellor,<br />Gurukula Kangri Vishwavidyalaya,Haridwar</h6>
				                </Media>
				            </Media>
							<p className="aboutprof">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>			        			            
				        </div>
						<div className="col-12 mt-5">
				            <Media tag="li">
				                <Media left middle>
				                    <Media top className=" mb-3 rounded-circle" object src= "/assets/images/faltu2.jpg" height="100px" width="100px"  alt= "name" />
				        		</Media>
				        			<Media body className="col-12">
				                    <Media heading><h3>Dr. Mayank Aggrwal</h3></Media>
				                    <h6>HOD OF Computer Science Department,<br />FET,Gurukula Kangri Vishwavidyalaya,Haridwar</h6>
				                </Media>
				            </Media>
							<p className="aboutprof">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>			        			            
				        </div>
				        <div className="col-12 mt-5">
				            <Media tag="li">
				                <Media left middle>
				                    <Media top className=" mb-3 rounded-circle" object src= "/assets/images/faltu3.jpg" height="100px" width="100px"  alt= "name" />
				        		</Media>
				        			<Media body className="col-12">
				                    <Media heading><h3>Dr. Sunil Panwar</h3></Media>
				                    <h6>HOD OF Physics Department,<br />FET,Gurukula Kangri Vishwavidyalaya,Haridwar</h6>
				                </Media>
				            </Media>
							<p className="aboutprof">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>			        			            
				        </div>
				        <div className="col-12 mt-5">
				            <Media tag="li">
				                <Media left middle>
				                    <Media top className=" mb-3 rounded-circle" object src= "/assets/images/faltu4.jpeg" height="100px" width="100px"  alt= "name" />
				        		</Media>
				        			<Media body className="col-12">
				                    <Media heading><h3>Dr. Chirag Patel</h3></Media>
				                    <h6>HOD of IDK Department,<br />FET,Gurukula Kangri Vishwavidyalaya,Haridwar</h6>
				                </Media>
				            </Media>
							<p className="aboutprof">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>			        			            
				        </div>
				</div>

				<div className="container">
					<div className="row">
						<div className="col-12 col-md-4 ">
							<Card>
				                <CardImg width="100%" src="/assets/images/img3.5.jpg" height="300px" width="100px" alt="name" />
				                    <CardBody>
				                    	<CardTitle>Students</CardTitle>
				                    	<CardText> Connect to your Colleageus, Senior and Juniors to interect and share! Sharing and connecting is definitly key to gain more! </CardText>
				                    	<Button color="success" href="student">Find Students</Button>
				                    </CardBody>
				            </Card>
				       </div>
				        <div className="col-12 col-md-4 ">
				            <Card>
				                <CardImg width="100%" src="/assets/images/img3.6.jpg" height="300px" width="100px" alt="name" />
				                    <CardBody>
				                    	<CardTitle>Students</CardTitle>
				                    	<CardText> Connect to your Professors, HOD's and better interect to gain more! Connect Directly amd ask them doubts anytime! </CardText>
				                    	<Button color="success" href="professor">Find Professors</Button>
				                    </CardBody>
				            </Card>
				        </div>
				        <div className="col-12 col-md-4 1">
				            <Card>
				                <CardImg width="100%" src="/assets/images/img3.7.jpg" height="300px" width="100px" alt="name" />
				                    <CardBody>
				                    	<CardTitle>Students</CardTitle>
				                    	<CardText>Connect to your Alumnies and interect to gain more! The more you connect, more you are exposed to experience. </CardText>
				                    	<Button color="success" href="alumni">find Alumnies</Button>
				                    </CardBody>
				            </Card>
				        </div>
				    </div>
				</div>
			</React.Fragment>

			);
	}
		
}


export default Home;