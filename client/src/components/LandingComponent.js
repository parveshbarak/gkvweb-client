import React, {Component} from 'react';
import { Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import '../Landing.css';


class Landing extends Component {
    constructor(props) {
        super(props);
    
        this.toggleNav = this.toggleNav.bind(this);
        this.state = {
          isNavOpen: false
        };
      }

      toggleNav() {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }

    render() {
        return(
        	<React.Fragment>
        	<div className="bg">
	            <div>
	                <Navbar expand="md">
	                    <div className="container">
	                        <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="50" width="150" alt='Ristorante Con Fusion' /></NavbarBrand>
	                        <NavbarToggler onClick={this.toggleNav} /><span className="navbar-toggler-icon"></span>
	                        <Collapse isOpen={this.state.isNavOpen} navbar>
	                            <Nav navbar className="ml-auto">
	                            <NavItem>
	                                <NavLink className="nav-link landing-links"  to='/signin' href="signin"><span className="fa fa-home fa-lg"></span> Login</NavLink>
	                            </NavItem>
	                            <NavItem>
	                                <NavLink className="nav-link landing-links"  to='/signup'><span className="fa fa-list fa-lg"></span> Signup</NavLink>
	                            </NavItem>
	                            </Nav>
	                        </Collapse>
	                    </div>
	                </Navbar>
	            </div>

				<div className = "row">
					<div className="col-2 col-md-4">
					</div>
					<div className="col-8 col-md-5 landing-clr">
						<h5>We help you connect!!</h5>
						<h2>GKVconnections</h2>
						<p>We help you connect to all your collageous, seniors, juniors,<br /> seniors, juniors, faculties and alumanies!</p>
						<div className="btns">
							<Button className="btn-spacing" color="primary" size="md" href="/signin">Login</Button>
							<Button className="btn-spacing" backgroiund-color="white" size="md" href="/signup">Signup</Button>
						</div>
					</div>
					<div className="col-2 col-md-3">
					</div>
				</div>
            </div>
            </React.Fragment>
        );
    }
}



export default Landing;