import React, { useState } from 'react';
import { Card, CardTitle, CardImg, CardBody,CardText, Button, Modal, Media } from 'reactstrap';
const AlumniCard = ({
  image,
  name,
  alumniId,
  role,
  year,
  description,
  branch,
  email,
  curricular,
  facebook,
  linkedin,
  github,
  twitter,
  instagram,
  native,
  whatsapp,

}) => {
  // States
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  return (
    <React.Fragment>
<Media tag="li" onClick= {toggle} className="cursor-change">
    <Media left middle>
        <Media object src={image} height="120px" width="120px" alt={name} />
    </Media>
    <Media body className="col-12 txt-clr">
        <Media heading>{name} </Media>
        <p>-- {branch} Department</p>
        <p>--{description}</p>
    </Media>    
</Media>

      <Modal className="modal-size" isOpen={modal} toggle={toggle}>
        <div className='modal-header d-flex justify-content-center'>
          <h5 className='modal-title text-center' id='exampleModalLabel'>
            {name}
          </h5>
          <button
            aria-label='Close'
            className='close'
            type='button'
            onClick={toggle}
          >
            <span aria-hidden={true}>X</span>
          </button>
        </div>
        <div className='modal-body'>
          <div className='d-flex justify-content-between ml-3'>
            <div className="row">
              <div className="col-md-5">
                <Card>
                  <CardImg src={image} alt={name} />
                </Card>
                <CardBody>
                  <CardTitle><h5>{name}</h5></CardTitle>
                  <CardText>{description}</CardText>
                </CardBody>
              </div>
              <div className="col-md-7">
                <div className="row  ml-1">
                    <h3> {name}</h3>
                </div>
                <div className="row  ml-2">
                    <h5> -- {branch} Department</h5>
                </div>
                <div className="row  ml-2">
                    <h5> -- {year} Year</h5>
                </div>
                <div className="row mt-3 ml-2">
                    <p> Intrested in Co-Curricular Activities :- {curricular} </p>
                </div>
                <div className="row mt-3 ml-2">
                    <p> {name} belongs to  <u> {native} </u> </p>
                </div>
                <ul className= "mt-3">
                    <li> E-Mail ID:- {email}</li>
                    <li> Linkedin Profile:- {linkedin}</li>
                    <li> Facebook Profile:- {facebook}</li>
                    <li> Github Profile:- {github}</li>
                    <li> Instagram Profile:- {instagram}</li>
                    <li> Twitter Profile :- {twitter}</li>
                    <li> Whatsapp No. :- {whatsapp}</li>
                </ul>
              </div>
              </div>
            </div>
        </div>
        <div className='modal-footer'>
          <div className='left-silde'>
            <a
              href={alumniId}
              className='btn-link'
              color='default'
              type='button'
              target='_blank'
              rel='noopener noreferrer'
            >
              Preview Link
            </a>
          </div>
          <div className='divider'></div>
          <div className='right-silde'>
            <a
              href={role}
              className='btn-link'
              color='default'
              type='button'
              target='_blank'
              rel='noopener noreferrer'
            >
              Info Link
            </a>
          </div>
        </div>
      </Modal>
    </React.Fragment>
  );
};

export default AlumniCard;