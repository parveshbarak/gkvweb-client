import React, {Component} from 'react';
import { Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron } from 'reactstrap';
import { NavLink, withRouter } from 'react-router-dom';
import { isAuth, signout } from '../auth/helpers';

 const Signout = ({ history }) => {
            const nav = () => (
                <ul>
                    {isAuth() && (
                            <li className="nav-item">
                                <span
                                    className="nav-link"
                                    style={{ cursor: 'pointer', color: '#fff' }}
                                     onClick={() => {
                                        signout(() => {
                                            history.push('/');
                                        });
                                    }}
                                >
                                    Signout
                                </span>
                            </li>
                        )}
                 </ul>
                );
            };



class Header extends Component {
    constructor(props) {
        super(props);
    
        this.toggleNav = this.toggleNav.bind(this);
        this.state = {
          isNavOpen: false
        };
      }

      toggleNav() {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }
    

   
    render() {
        return(
            <div>
                <Navbar dark expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="50" width="150" alt='Ristorante Con Fusion' /></NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/student'><span className="fa fa-list fa-lg"></span> Students</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/professor'><span className="fa fa-info fa-lg"></span> Professors</NavLink>
                            </NavItem>                            
                            <NavItem>
                                <NavLink className="nav-link" to='/alumni'><span className="fa fa-address-card fa-lg"></span> Alumnies</NavLink>
                            </NavItem>                            
                            <NavItem>
                                {withRouter(Signout)}
                            </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>

            </div>
        );
    }
}


export default Header;