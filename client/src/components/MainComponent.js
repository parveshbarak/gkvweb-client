import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Navbar, NavbarBrand } from 'reactstrap';
import Student from './StudentComponent';
import Professor from './ProfessorComponent';
import Alumni from './AlumniComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import Header from './HeaderComponent';
import HomeHeader from './HeaderHomeComponent';
import Footer from './FooterComponent';

import { isAuth, signout } from '../auth/helpers';
import Landing from './LandingComponent';
import Layout from '../core/Layout'
import Private from '../core/Private'


class Main extends Component {

  constructor(props) {
    super(props);
  }


  render() {

if (isAuth()){
 	return (
	     <div>
	     < Header />
	     < HomeHeader />
        
          <Switch>
              <Route path='/home' component={Home} />
              <Route exact path='/student' component={Student} />} />
              <Route exact path='/professor' component={Professor} />
              <Route exact path='/alumni' component={Alumni} />
              <Route exact path='/contactus' component={Contact} />
             <Route exact path="/aboutus" component={About} />
             <Route exact path="/private" Component = {Private} />
              <Redirect to="/home" />
          </Switch>
          <Footer />
      </div>
    );
  }

   else {
    return(
      <Route path='/' component={Landing} />
      
      );
  }

}
}

export default Main;