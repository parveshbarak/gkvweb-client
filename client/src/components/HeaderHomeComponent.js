import React, {Component} from 'react';
import { Jumbotron, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';

class HomeHeader extends Component {
	constructor (props) {
		super(props);
	}

	render() {
		return(
			    <Jumbotron className="text-center" >
				    <div className="container text-center">
						<h1 className="display">GKVconnections</h1>
				        <p>We connect and come together to bring the change!</p>
				          	<Button className="padding" color="primary" size="sm" href="profile">Visit Profile</Button> <span></span>
      						<Button className="padding" color="success" size="sm" href="private">Update Profile</Button>
				    		
                    </div>
                </Jumbotron> 
			);
	}
}

export default HomeHeader;