import React, {Component, useState } from 'react';
import { Card, CardTitle, CardImg, CardBody, Button, Modal, Breadcrumb,BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import StudentCard from './StudentCard';

class Student extends Component {

  constructor(props){    
    super(props);
    this.state = {
      users: {},
      isLoaded: false,
    }
  }

  toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

        
    

componentDidMount() {
   fetch('http://localhost:8000/api/students')
    .then(res => res.json())
    .then(json => {
      this.setState({
        isLoaded:true,
        users: json,
      })
    });

}

render() {

  var {isLoaded, users } = this.state;

  if(!isLoaded){
      return(
          <div><h3>Loading...</h3></div>
        );
  } 

  else {

    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Student</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Student</h3>
                    <hr />
                </div>                 
            </div>
            <div className="row"> 
              {users.map(user => (
                <div key={user._id} >
                  <StudentCard
                      image ={user.image}
                      role= {user.role} 
                      name= {user.name}
                      studentId = {user._id}
                      branch= {user.branch}
                      year= {user.year}
                      email = {user.email}
                      curricular = {user.curricular}
                      facebook = {user.facebook}
                      github = {user.github}
                      instagram = {user.instagram}
                      linkedin = {user.linkedin}
                      native = {user.native}
                      twitter = {user.twitter}
                      whatsapp = {user.whatsapp}
                      description = {user.description}
                    />
                </div>
              ))};
            </div>
        </div>
      
    );
}
}
}
export default Student;