import React, { useState } from 'react';
import { Link, Redirect, NavLink } from 'react-router-dom';
import Layout from '../core/Layout';
import axios from 'axios';
import { isAuth } from './helpers';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron, Button, Label, Input } from 'reactstrap';


const Signup = () => {
    const [values, setValues] = useState({
        name: 'Ryan',
        email: 'kaloraatjs@gmail.com',
        password: 'rrrrrr',
        role: 'Student',
        buttonText: 'Submit'
    });

    const { name, email, password, role, buttonText } = values;

    const handleChange = name => event => {
        // console.log(event.target.value);
        setValues({ ...values, [name]: event.target.value });
    };

    const clickSubmit = event => {
        event.preventDefault();
        setValues({ ...values, buttonText: 'Submitting' });
        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_API}/signup`,
            data: { name, email, password, role }
        })
            .then(response => {
                console.log('SIGNUP SUCCESS', response);
                setValues({ ...values, name: '', email: '', password: '', role: '', buttonText: 'Submitted' });
                toast.success(response.data.message);
            })
            .catch(error => {
                console.log('SIGNUP ERROR', error.response.data);
                setValues({ ...values, buttonText: 'Submit' });
                toast.error(error.response.data.error);
            });
    };

    const signupForm = () => (
        <form>
            <div className="form-group">
                <label className="label-clr">Name</label>
                <input onChange={handleChange('name')} value={name} type="text" className="form-control" />
            </div>

            <div className="form-group">
                <label className="label-clr">Email</label>
                <input onChange={handleChange('email')} value={email} type="email" className="form-control" />
            </div>
            
            <Label className="label-clr" for="role">Role</Label>
            <Input onChange={handleChange('role')} value={role} type="select" name="select" id="role">
              <option>Student</option>
              <option>Professor</option>
              <option>Alumni</option>
            </Input>

            <div className="form-group">
                <label className="label-clr">Password</label>
                <input onChange={handleChange('password')} value={password} type="password" className="form-control" />
            </div>

            <div>
                <button className="btn btn-primary" onClick={clickSubmit}>
                    {buttonText}
                </button>
            </div>
        </form>
    );

    return (
        <Layout>
            <div className="signup-bg">
                    <Navbar expand="md">
                        <div className="container">
                            <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="50" width="150" alt='Ristorante Con Fusion' /></NavbarBrand>
                                <Nav navbar className="ml-auto">
                                <NavItem>
                                    <NavLink className="nav-link landing-links"  to='/signin' href="signin"><span className="fa fa-home fa-lg"></span> Login</NavLink>
                                </NavItem>
                                <NavItem> 
                                    <NavLink className="nav-link landing-links"  to='/signup'><span className="fa fa-list fa-lg"></span> Signup</NavLink>
                                </NavItem>
                                </Nav>
                        </div>
                    </Navbar>
                <div className="col-md-6 offset-md-3">
                    <ToastContainer />
                    {isAuth() ? <Redirect to="/" /> : null}
                    <h1 className="p-5 text-center">Signup</h1>
                    {signupForm()}
                    <br /> 
                    <h6 className="label-clr">Need Help?</h6>
                    <Link to="/auth/password/forgot" className="btn btn-sm btn-outline-danger">
                        Forgot Password
                    </Link>
                </div>
            </div>
        </Layout>
    );
};

export default Signup;