  
import React, { useState } from 'react';
import { Link, Redirect, NavLink  } from 'react-router-dom';
import Layout from '../core/Layout';
import axios from 'axios';
import { authenticate, isAuth } from './helpers';
import { ToastContainer, toast } from 'react-toastify';
import Google from './Google';
import Facebook from './Facebook';
import 'react-toastify/dist/ReactToastify.min.css';
import { Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem, Jumbotron, Button } from 'reactstrap';

const Signin = ({ history }) => {
    const [values, setValues] = useState({
        email: 'kaloraatjs@gmail.com',
        password: 'rrrrrr',
        buttonText: 'Submit'
    });

    const { email, password, buttonText } = values;

    const handleChange = name => event => {
        // console.log(event.target.value);
        setValues({ ...values, [name]: event.target.value });
    };

    const informParent = response => {
        authenticate(response, () => {
            isAuth() && isAuth().role === 'admin' ? history.push('/admin') : history.push('/private');
        });
    };

    const clickSubmit = event => {
        event.preventDefault();
        setValues({ ...values, buttonText: 'Submitting' });
        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_API}/signin`,
            data: { email, password }
        })
            .then(response => {
                console.log('SIGNIN SUCCESS', response);
                // save the response (user, token) localstorage/cookie
                authenticate(response, () => {
                    setValues({ ...values, name: '', email: '', password: '', buttonText: 'Submitted' });
                     toast.success(`Hey ${response.data.user.name}, Welcome back!`);
                    isAuth() && isAuth().role === 'admin' ? history.push('/admin') : history.push('/private');
                });
            })
            .catch(error => {
                console.log('SIGNIN ERROR', error.response.data);
                setValues({ ...values, buttonText: 'Submit' });
                toast.error(error.response.data.error);
            });
    };

    const signinForm = () => (
        <form>
            <div className="form-group">
                <label className="text-muted">Email</label>
                <input onChange={handleChange('email')} value={email} type="email" className="form-control" />
            </div>

            <div className="form-group">
                <label className="text-muted">Password</label>
                <input onChange={handleChange('password')} value={password} type="password" className="form-control" />
            </div>

            <div>
                <button className="btn btn-primary" onClick={clickSubmit}>
                    {buttonText}
                </button>
            </div>
        </form>
    );



    return (
        <Layout>
            <div className="signin-bg">
                    <Navbar expand="md">
                        <div className="container">
                            <NavbarBrand className="mr-auto" href="/"><img src='assets/images/logo.png' height="50" width="150" alt='Ristorante Con Fusion' /></NavbarBrand>
                                <Nav navbar className="ml-auto">
                                <NavItem>
                                    <NavLink className="nav-link landing-links"  to='/signin' href="signin"><span className="fa fa-home fa-lg"></span> Login</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className="nav-link landing-links"  to='/signup'><span className="fa fa-list fa-lg"></span> Signup</NavLink>
                                </NavItem>
                                </Nav>
                        </div>
                    </Navbar>
                <div className="col-md-6 offset-md-3">
                    <ToastContainer />
                    {isAuth() ? <Redirect to="/" /> : null}
                    <h1 className="p-5 text-center">Signin</h1>
                    <Google informParent={informParent} />
                    {signinForm()}
                    <br />
                    <Link to="/auth/password/forgot" className="btn btn-sm btn-outline-danger">
                        Forgot Password
                    </Link>
                </div>
            </div>
        </Layout>
    );
};

export default Signin;