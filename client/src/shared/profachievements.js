export const PROFACHIEVEMENTS = 
[
	{
		id: 0,
		professorId: 0,
		image: '/assets/images/faltu3.1.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 1,
		professorId: 0,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 2,
		professorId: 0,
		image: '/assets/images/faltu3.6.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 3,
		professorId: 1,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 4,
		professorId: 1,
		image: '/assets/images/faltu3.3.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 5,
		professorId: 2,
		image: '/assets/images/faltu3.4.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 6,
		professorId: 2,
		image: '/assets/images/faltu3.6.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 7,
		professorId: 2,
		image: '/assets/images/faltu3.5.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 8,
		professorId: 3,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 9,
		professorId: 3,
		image: '/assets/images/faltu3.1.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 10,
		professorId: 4,
		image: '/assets/images/faltu3.4.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 11,
		professorId: 4,
		image: '/assets/images/faltu3.7.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 12,
		professorId: 4,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 13,
		professorId: 5,
		image: '/assets/images/faltu3.3.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 14,
		professorId: 5,
		image: '/assets/images/faltu3.5.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	}
];