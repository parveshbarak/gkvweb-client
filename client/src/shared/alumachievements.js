export const ALUMACHIEVEMENTS = 
[
	{
		id: 0,
		alumniId: 0,
		image: '/assets/images/faltu3.1.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 1,
		alumniId: 0,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 2,
		alumniId: 0,
		image: '/assets/images/faltu3.6.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 3,
		alumniId: 1,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 4,
		alumniId: 1,
		image: '/assets/images/faltu3.3.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 5,
		alumniId: 2,
		image: '/assets/images/faltu3.4.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 6,
		alumniId: 2,
		image: '/assets/images/faltu3.6.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 7,
		alumniId: 2,
		image: '/assets/images/faltu3.5.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 8,
		alumniId: 3,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 9,
		alumniId: 3,
		image: '/assets/images/faltu3.1.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 10,
		alumniId: 4,
		image: '/assets/images/faltu3.4.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 11,
		alumniId: 4,
		image: '/assets/images/faltu3.7.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 12,
		alumniId: 4,
		image: '/assets/images/faltu3.2.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 13,
		alumniId: 5,
		image: '/assets/images/faltu3.3.png',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	},
	{
		id: 14,
		alumniId: 5,
		image: '/assets/images/faltu3.5.jpg',
		about: 'lorem epsum is simplly dummy text of printing and type testing industry.',
	}
];