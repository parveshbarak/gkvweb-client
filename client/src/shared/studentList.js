import React, {Component} from 'react';

class StudentList extends Component {

  constructor(props){    
    super(props);
    this.state = {
      users: {},
      isLoaded: false,
    }
  }

componentDidMount() {
   fetch('http://localhost:8000/api/students')
    .then(res => res.json())
    .then(json => {
      this.setState({
        isLoaded:true,
        users: json,
      })
    });

}

render() {

  var {isLoaded, users} = this.state;

  if(!isLoaded){
      return(
          <div>Loading...</div>
        );
  }

  else {
      return (
      <div className="App">
          
        <ul>
          {users.map(user => (
              <li key={user._id}>
				Role: {user.role},  
                NAME:{user.name},
				email: {user.email} ,				
				about: {user.achievements}
				
			  </li>
            ))};
        </ul>
      </div>
    ); 
  }

}
}

export default StudentList;












//
//[
//{
    //id: 0,
    //name: 'Abhishek Kumar',
    //email: 'abkumar@gmail.com',
    //image: '/assets/images/faltu2.5.jpeg',
    //role: 'Professor',
    //position: 'Assistant Professor',
    //branch: 'CSE',
    //native : 'Lucknow, UP',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'of letters as opposed to using conteent here making it look like readable english.Many desktop publishing publishing packages and web package editors noe use lorem ipsum as their defalt model text',
    //linkedin: 'abkumar@linkedin',
    //facebook:'abkumar@facebook',
    //github : 'abkumar@github',
    //instagran: 'abkumar@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
    //},
//
    //{
    //id: 1,
    //name: 'Gulshan Gupta',
    //email: 'johnny@gmail.com',
    //image: '/assets/images/faltu2.6.jpeg',
    //role: 'Professor',
    //position: 'Head of Department',
    //branch: 'Electrical Engineering',
    //native : 'Jalalabaad, UP',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'lorem it was long established fact that a reader will distracted bynthe readable  content of page when looking aat its layout. The point of using lorem ipsum it has a more or less distribution ',
    //linkedin: 'johnny@linkedin',
    //facebook:'johnny@facebook',
    //github : 'johnny@github',
    //instagran: 'johnny@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
    //},
//
    //{
    //id: 2,
    //name: 'Utsav Shukla',
    //email: 'laundabazz@gamil.com',
    //image: '/assets/images/faltu2.2.jpeg',
    //role: 'Professor',
    //position: 'Assistant Professor',
    //branch: 'Mechanical Engineering',
    //native : 'Lucknow, UP',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'of letters as opposed to using conteent here making it look like readable english.Many desktop publishing publishing packages and web package editors noe use lorem ipsum as their defalt model text',
    //linkedin: 'laundabazz@linkedin',
    //facebook:'laundabazz@facebook',
    //github : 'abkumar@github',
    //instagran: 'abkumar@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
    //},
//
    //{
    //id: 3,
    //name: 'Shabbir Poswal',
    //email: 'miabhai@gmail.com',
    //image: '/assets/images/faltu2.12.jpeg',
    //role: 'Professor',
    //position: ' Assistant Professor',
    //branch: 'CSE',
    //native : 'Hamirpur, himachal',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'lorem it was long established fact that a reader will distracted bynthe readable  content of page when looking aat its layout. The point of using lorem ipsum it has a more or less distribution ',
    //linkedin: 'miabhai@linkedin',
    //facebook:'miabhai@facebook',
    //github : 'miabhai@github',
    //instagran: 'miabhai@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
    //},
//
    //{
    //id: 4,
    //name: 'Nachiketra Pandey ',
    //email: 'kunal@gmail.com',
    //image: '/assets/images/faltu2.1.jpeg',
    //role: 'Professor',
    //position: 'Assistant Professor',
    //branch: 'Electronics Enginerring',
    //native : 'Behraich, UP',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'of letters as opposed to using conteent here making it look like readable english.Many desktop publishing publishing packages and web package editors noe use lorem ipsum as their defalt model text',
    //linkedin: 'kunal@linkedin',
    //facebook:'kunal@facebook',
    //github : 'abkumar@github',
    //instagran: 'abkumar@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
    //},
//
    //{
    //id: 5,
    //name: 'Harshit Ojha',
    //email: 'beta@gmail.com',
    //image: '/assets/images/faltu2.3.jpeg',
    //role: 'Professor',
    //position: 'Assistant Professor',
    //branch: 'Electrical Engineering',
    //native : 'Kanpur, UP',
    //qualification: 'M.tech Computer Science, PHD Cloud services',
    //description: 'lorem it was long established fact that a reader will distracted bynthe readable  content of page when looking aat its layout. The point of using lorem ipsum it has a more or less distribution ',
    //linkedin: 'beta@linkedin',
    //facebook:'beta@facebook',
    //github : 'abkumar@github',
    //instagran: 'abkumar@instagram',
    //twitter : 'abkumar@twitter',
    //mobile: '999999'
  //}
//
  //];